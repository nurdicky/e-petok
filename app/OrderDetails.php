<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
  protected $fillable = [
    'orders_id',
    'products_id',
    'quantity',
    'price',
    'sub_total',
  ];

  public $timestamps = false;

  public function orders()
  {
    return $this->belongsTo('App\Orders', 'orders_id');
  }

  public function products()
  {
    return $this->belongsTo('App\Products', 'products_id');
  }


}
