<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
  protected $fillable = [
    'products_name',
    'products_price',
    'products_stock',
    'products_category',
    'products_image',
  ];

  public function categories()
  {
		return $this->belongsTo('App\Categories', 'products_category');
	}

  public function orderDetails()
  {
		return $this->hasMany('App\OrderDetails', 'products_id', 'id');
	}

}
