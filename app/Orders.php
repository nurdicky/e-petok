<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
  protected $fillable = [
    'orders_code',
    'user_id',
    'status',
    'total_ammount',
  ];

  public $timestamps = false;

  public function orderDetails()
  {
		return $this->hasMany('App\OrderDetails', 'orders_id', 'id');
	}

  public function users()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

}
