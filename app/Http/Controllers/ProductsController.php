<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {

    }

    public function detail($id)
    {
      // $products = Products::find($id);
      $products  = Products::with('categories')->find($id);

      return view('detail', ['products'=> $products ]);
    }
}
