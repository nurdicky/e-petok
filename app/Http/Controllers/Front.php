<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Front extends Controller
{
    public function index()
    {
      return view('home',  array('page' => 'home') );
    }

    public function login()
    {
      return view('login',  array('page' => 'home') );
    }

    public function register()
    {
      return view('register',  array('page' => 'home') );
    }

    public function logout()
    {
      return view('login',  array('page' => 'home') );
    }

    public function products()
    {
      return view('products',  array('page' => 'products') );
    }

    public function products_details($id)
    {
      return view('products_details',  array('page' => 'products') );
    }

    public function products_category($name)
    {
      return view('products',  array('page' => 'products') );
    }

    public function products_popular()
    {
      return view('products',  array('page' => 'products') );
    }

    public function cart()
    {
      return view('cart',  array('page' => 'home') );
    }

    public function checkout()
    {
      return view('checkout',  array('page' => 'home') );
    }

    public function search($query)
    {
      return view('products',  array('page' => 'products') );
    }
}
