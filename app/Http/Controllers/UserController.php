<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function index()
    {
      # code...
    }

    public function update(Request $req, $id)
    {
      $user = DB::table('users')->where('password', $req->password)->first();
      if ($user) {
        $pass = $req->password;
      }
      else{
        $pass = bcrypt($req->password);
      }

      $users = User::find($id);
      $users->name = $req->name;
      $users->fullname = $req->fullname;
      $users->email = $req->email;
      $users->password = $pass;
      $users->phone = $req->phone;
      $users->address = $req->address;
      $users->save();


      return Response::json($pass);
    }
}
