<?php

namespace App\Http\Controllers\Admin;

use App\OrderDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class OrderDetailsController extends Controller
{
  function __construct()
  {
    date_default_timezone_set("Asia/Bangkok");
  }

  public function index()
  {
    return view('admin.orders_details');
  }

  public function view_all()
  {
    $orders = OrderDetails::all();
    return view('admin.orders_details', ['orders' => $orders]);
  }

  public function view_item($id)
  {
    $orders = OrderDetails::find($id);
    return Response::json($orders);
  }

  public function create(Request $req)
  {
    $orders = OrderDetails::create($req->all());
    return Response::json($orders);
  }

  public function update(Request $req, $id)
  {
    $orders = OrderDetails::find($id);
    $orders->orders_id = $req->orders_id;
    $orders->products_id = $req->products_id;
    $orders->quantity = $req->quantity;
    $orders->price = $req->price;
    $orders->sub_total = $req->sub_total;
    $orders->save();
    return Response::json($orders);
  }

  public function destroy($id)
  {
    $orders = OrderDetails::destroy($id);
    return Response::json($orders);
  }
}
