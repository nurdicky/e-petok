<?php

namespace App\Http\Controllers\Admin;

use App\Orders;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
      $countOrders = Orders::where('status', 'Selesai')->count();
      $countOrdersProses = Orders::where('status', '!=' ,'Selesai')->count();

      $users = User::whereHas('orders', function ($query) {
          $query->where('status', 'Selesai');
      })->with('orders')->get();

      // foreach ($users as $user) {
      //   $total = 0;
      //   foreach($user->orders as $order){
      //     $amount = $order->total_amount;
      //     $total = (Integer) $total + $amount;
      //     dd($total );
      //   }
      // }

      return view('admin.dashboard', [
        'countOrders' => $countOrders,
        'countOrdersProses' => $countOrdersProses,
        'users' => $users
      ] );
    }
}
