<?php

namespace App\Http\Controllers\Admin;

use App\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
  function __construct()
  {
    date_default_timezone_set("Asia/Bangkok");
  }

  public function index()
  {
    return view('admin.orders');
  }

  public function view_all()
  {
    $orders = Orders::all();
    return view('admin.orders', ['orders' => $orders]);
  }

  public function view_item($id)
  {
    $orders = Orders::find($id);
    return Response::json($orders);
  }

  public function create(Request $req)
  {
    // $orders = Orders::create($req->all());
    $orders = new Orders;
    $orders->user_id = $req['user_id'];
    $orders->status = $req['status'];
    $orders->total_amount = $req['total_amount'];
    $orders->orders_code = 'ORD0000';
    $orders->save();

    $id = $orders->id;
    $order = Orders::find($id);
    $char = 'ORD';
    $newkode = $char.sprintf("%04s",$id);
    $order->orders_code = $newkode;
    $order->save();

    return Response::json($order);
  }

  public function update(Request $req, $id)
  {
    $orders = Orders::find($id);
    $orders->orders_date = date('Y-m-d H:i:s');
    $orders->user_id = $req->user_id;
    $orders->status = $req->status;
    $orders->total_amount = $req->total_amount;
    $orders->save();
    return Response::json($orders);
  }

  public function destroy($id)
  {
    $orders = Orders::destroy($id);
    return Response::json($orders);
  }
}
