<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
      return view('admin.category');
    }

    public function view_all()
    {
      $category = Categories::all();
      return view('admin.category', ['categories' => $category]);
    }

    public function view_item($id)
    {
      $category = Categories::find($id);
      return Response::json($category);
    }

    public function create(Request $req)
    {
      $category = Categories::create($req->all());
      return Response::json($category);
    }

    public function update(Request $req, $id)
    {
      $category = Categories::find($id);
      $category->category_name = $req->category_name;
      $category->save();
      return Response::json($category);
    }

    public function destroy($id)
    {
      $category = Categories::destroy($id);
      return Response::json($category);
    }
}
