<?php

namespace App\Http\Controllers\Admin;

use App\Products;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
      return view('admin.products');
    }

    public function view_all()
    {
      // $products = Products::all();
      $products = Products::all();
      return view('admin.products', ['products' => $products]);
      // return view('admin.products', compact('products'));
    }

    public function view_item($id)
    {
      $products = Products::find($id);
      return Response::json($products);
    }

    public function create(Request $req)
    {
      if ($req->products_id) {
        $input = $req->file('products_image');
        $input = $req->all();
        if (@!$input['products_image']) {
          $input['products_image'] = $req->tempImg;
        }else{
          $input['products_image'] = time().'.'.$req->products_image->getClientOriginalExtension();
          $req->products_image->move(public_path('images/products'), $input['products_image']);
        }

        $this->update(
          $req->products_name,
          $req->products_price,
          $req->products_stock,
          $req->products_category,
          $input['products_image'],
          $req->products_id);
      }else{
        $input = $req->file('products_image');
        $input = $req->all();
        $input['products_image'] = time().'.'.$req->products_image->getClientOriginalExtension();
        $req->products_image->move(public_path('images/products'), $input['products_image']);

        Products::create($input);
      }

      return redirect()->intended('admin/products');
      // return Response::json(['success'=>'done', 'data' => $input ]);

    }

    public function update($name, $price, $stock, $category, $image,  $id)
    {
      $products = Products::find($id);
      $products['products_name'] = $name;
      $products['products_price'] = $price;
      $products['products_stock'] = $stock;
      $products['products_category'] = $category;
      $products['products_image'] = $image;
      $products->save();

      return Response::json($products);
    }

    public function destroy($id)
    {
      $products = Products::destroy($id);
      return Response::json($products);
    }
}
