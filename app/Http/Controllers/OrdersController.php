<?php

namespace App\Http\Controllers;

use App\Orders;
use App\OrderDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class OrdersController extends Controller
{
    public function index()
    {
      # code...
    }

    public function view_all_cart()
    {
      $allOrders = Orders::whereHas('users', function ($query) {
          $query->where('id', Auth::user()->id);
      })->with('users')->get();

      $carts = OrderDetails::whereHas('orders', function ($query) {
          $query->where('user_id', Auth::user()->id);
          $query->where('status', 'Pembayaran');
      })->with('orders', 'products')->get();

      //dd($carts);
      return view('cart', ['carts' => $carts ]);
    }
}
