<?php

namespace App\Http\Controllers;

use Validator;
use App\Tasks;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::all();
        return view('home', [ 'products' => $products ]);
    }

    public function logout() {
        Auth::logout();

        return redirect()->intended('login');
    }

    public function task_create(Request $request)
    {
      $task = Tasks::create($request->all());
      return Response::json($task);
    }

    public function check_file(Request $req)
    {
      // $validator = Validator::make($req->all(), [
      //   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      // ]);
      //
      // if ($validator->passes()) {

        $input = $req->all();
        $input['image'] = time().'.'.$req->image->getClientOriginalExtension();
        $req->image->move(public_path('images/products'), $input['image']);

        // AjaxImage::create($input);

        return redirect()->intended('admin/products');
        // return response()->json(['success'=>'done', 'image' => $input['image']]);
      // }
      //
      // return response()->json(['error'=>$validator->errors()->all()]);
    }
}
