<?php

use App\Tasks;
use App\Products;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*---  PUBLIC ROUTING  ----*/
Route::get('/', function (){
  return redirect()->route('home');
});
Route::get('/products', 'Front@products');
Route::get('/products/detail/{id}', 'ProductsController@detail');
Route::get('/products/category', 'Front@products_category');
Route::get('/products/popular', 'Front@products_popular');
Route::get('/cart', 'OrdersController@view_all_cart');
Route::get('/checkout', [
    'middleware' => 'auth',
    'uses' => 'Front@checkout'
]);

Route::get('/search/{query}', 'Front@search');
Route::get('/panduan', function(){
  return view('panduan');
});
Route::get('/investasi', function(){
  return view('investasi');
});
Route::get('/about', function(){
  return view('about');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');

/*---  PRIVATE ROUTING  ----*/
Route::group(['middleware' => 'admin'], function () {
    Route::get('admin/dashboard', [
      'as' => 'dashboard',
      'uses' => 'Admin\DashboardController@index'
    ]);
    //profile
    Route::get('/admin/profile', function(){
      return view('admin.profile');
    });
    Route::put('/admin/profile/{id}', 'UserController@update');
    //products
    Route::get('/admin/products', 'Admin\ProductsController@view_all' );
    Route::post('/admin/products', 'Admin\ProductsController@create' );
    Route::get('/admin/products/{products_id}', 'Admin\ProductsController@view_item');
    Route::put('/admin/products/{products_id}', 'Admin\ProductsController@update');
    Route::delete('/admin/products/{products_id}', 'Admin\ProductsController@destroy');
    //Categories
    Route::get('/admin/category', 'Admin\CategoryController@view_all' );
    Route::post('/admin/category', 'Admin\CategoryController@create' );
    Route::get('/admin/category/{id}', 'Admin\CategoryController@view_item' );
    Route::put('/admin/category/{id}', 'Admin\CategoryController@update' );
    Route::delete('/admin/category/{id}', 'Admin\CategoryController@destroy' );
    //Orders Details
    Route::get('/admin/orders/detail', 'Admin\OrderDetailsController@view_all' );
    Route::post('/admin/orders/detail', 'Admin\OrderDetailsController@create' );
    Route::get('/admin/orders/detail/{id}', 'Admin\OrderDetailsController@view_item' );
    Route::put('/admin/orders/detail/{id}', 'Admin\OrderDetailsController@update' );
    Route::delete('/admin/orders/detail/{id}', 'Admin\OrderDetailsController@destroy' );
    //Orders
    Route::get('/admin/orders', 'Admin\OrdersController@view_all' );
    Route::post('/admin/orders', 'Admin\OrdersController@create' );
    Route::get('/admin/orders/{id}', 'Admin\OrdersController@view_item' );
    Route::put('/admin/orders/{id}', 'Admin\OrdersController@update' );
    Route::delete('/admin/orders/{id}', 'Admin\OrdersController@destroy' );


    //Tasks (Percobaan)
    Route::get('/admin/tasks/{task_id}',function($task_id){
        $task = Tasks::find($task_id);

        return Response::json($task);
    });
    Route::post('/admin/tasks', 'HomeController@task_create');
    Route::get('/admin/tasks', function(){
      $tasks = Tasks::all();
        //return View::make('welcome')->with('tasks',$tasks);
      return view('admin.tasks', ['tasks' => $tasks]);
    });
    Route::put('/admin/tasks/{task_id}',function(Request $request, $task_id){
        $task = Tasks::find($task_id);

        $task->task = $request->task;
        $task->description = $request->description;

        $task->save();

        return Response::json($task);
    });
    Route::delete('/admin/tasks/{task_id}', function($task_id){

        $task = Tasks::destroy($task_id);

        return Response::json($task);
    });

});
