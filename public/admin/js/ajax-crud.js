var host = window.location.hostname;
var path = window.location.pathname;

$(document).ready(function(){

    var url = "";
    if (path) {
      url = path;
    }
    console.log(url);

    //display modal form for task editing
    $('.open-modal').click(function(){
        var id = $(this).val();
        $.get(url + '/' + id, function (data) {
            //success data
            console.log(data);
            if (path.search('admin/products') > 0 ) {
              $('#products_id').val(data.id);
              $('#products_name').val(data.products_name);
              $('#products_price').val(data.products_price);
              $('#products_stock').val(data.products_stock);
              $('#products_category').val(data.products_category);
              $('#tempImg').val(data.products_image);
              $('#btn-save').val("update");
              $('#imageView').show();
              $('#imageView').attr('src', '/images/products/'+data.products_image);
            }
            else if (path.search('admin/tasks') > 0 ) {
              $('#task_id').val(data.id);
              $('#task').val(data.task);
              $('#description').val(data.description);
              $('#btn-save').val("update");
            }
            else if (path.search('admin/category') > 0 ) {
              $('#category_id').val(data.id);
              $('#category_name').val(data.category_name);
              $('#btn-save').val("update");
            }
            else if (path.search('admin/orders/detail') > 0 ) {
              $('#orders_id').val(data.id);
              $('#order_id').val(data.orders_id);
              $('#products_id').val(data.products_id);
              $('#quantity').val(data.quantity);
              $('#price').val(data.price);
              $('#sub_total').val(data.sub_total);
              $('#btn-save').val("update");
            }
            else if (path.search('admin/orders') > 0 ) {
              $('#orders_id').val(data.id);
              $('#status').val(data.status);
              $('#user_id').val(data.user_id);
              $('#total_amount').val(data.total_amount);
              $('#btn-save').val("update");
            }

            $('#myModal').modal('show');

        })
    });

    //display modal form for creating new task
    $('#btn-add').click(function(){
        if (path.search('admin/products') > 0) {
          $('#btn-save').attr('type', 'submit');
          $('#imageView').hide();
        }
        $('#btn-save').val("add");
        $('#frmTasks').trigger("reset");
        $('#frmProducts').trigger("reset");
        $('#frmCategory').trigger("reset");
        $('#frmOrders').trigger("reset");
        $('#myModal').modal('show');
    });

    //delete task and remove it from list
    $('.delete-item').click(function(e){
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        e.preventDefault();

        if (path.search('admin/products') > 0 ) {
          var id = $(this).val();
        }
        else if (path.search('admin/tasks') > 0 ) {
          var id = $(this).val();
        }
        else if (path.search('admin/category') > 0 ) {
          var id = $(this).val();
        }
        else if (path.search('admin/orders/detail') > 0 ) {
          var id = $(this).val();
        }
        else if (path.search('admin/orders') > 0 ) {
          var id = $(this).val();
        }

        $.ajax({
            type: "DELETE",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                if (path.search('admin/products') > 0 ) {
                  $("#product" + id).remove();
                }
                else if (path.search('admin/tasks') > 0 ) {
                  $("#task" + id).remove();
                }
                else if (path.search('admin/category') > 0 ) {
                  $("#category" + id).remove();
                }
                else if (path.search('admin/orders/detail') > 0 ) {
                  $("#orders" + id).remove();
                }
                else if (path.search('admin/orders') > 0 ) {
                  $("#orders" + id).remove();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    //create new task / update existing task

    if (path.search('admin/products') > 0 ) {

    }else{
      console.log("Masuk create");
      $("#btn-save").click(function (e) {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
          })

          e.preventDefault();

          if (path.search('admin/category') > 0) {
            var formData = {
                category_name: $('#category_name').val(),
            }
            // used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new resource
            var id = $('#category_id').val();;
            var my_url = url;
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + id;
            }
          }
          else if (path.search('admin/tasks') > 0) {
            var formData = {
                task: $('#task').val(),
                done: 1,
                description: $('#description').val(),
            }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new resource
            var id = $('#task_id').val();;
            var my_url = url;
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + id;
            }
          }
          else if (path.search('admin/profile') > 0) {
            var formData = {
                name: $('#name').val(),
                fullname: $('#fullname').val(),
                email: $('#email').val(),
                password: $('#pass').val(),
                phone: $('#phone').val(),
                address: $('#address').val(),
            }

            var state = $('#btn-save').val();

            var type = "POST"; //for creating new resource
            var id = $('#user_id').val();;
            var my_url = url;
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + id;
            }
          }
          else if (path.search('admin/orders/detail') > 0) {
            var formData = {
              orders_id: $('#order_id').val(),
              products_id: $('#products_id').val(),
              quantity: $('#quantity').val(),
              price: $('#price').val(),
              sub_total: $('#sub_total').val(),
            }
            // used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new resource
            var id = $('#orders_id').val();;
            var my_url = url;
            if (state == "update"){
              type = "PUT"; //for updating existing resource
              my_url += '/' + id;
            }
          }
          else if (path.search('admin/orders') > 0) {
            var formData = {
                user_id: $('#user_id').val(),
                status: $('#status').val(),
                total_amount: $('#total_amount').val(),
            }
            // used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new resource
            var id = $('#orders_id').val();;
            var my_url = url;
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + id;
            }
          }

          console.log(formData);

          $.ajax({
              type: type,
              url: my_url,
              data: formData,
              dataType: "json",
              success: function (data) {
                  console.log(data);

                  if (path.search('admin/products') > 0) {
                    var product = '';
                    product = '<tr id="product' + data.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + data.products_name + '</td><td>' + data.products_price + '</td><td>' + data.products_stock + '</td><td>' + data.products_category + '</td>';
                    product += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + data.id + '">Edit</button> ';
                    product += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add"){ //if user added a new record
                        $('#tabel-list').append(product);
                    }else{ //if user updated an existing record

                        $("#product" + id).replaceWith( product );
                    }

                    $('#frmProducts').trigger("reset");
                  }
                  else if (path.search('admin/tasks') > 0) {
                    var task = '';
                    task = '<tr id="task' + data.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + data.task + '</td><td>' + data.description + '</td><td>' + data.created_at + '</td>';
                    task += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + data.id + '">Edit</button> ';
                    task += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add"){ //if user added a new record
                        $('#tasks-list').append(task);
                    }else{ //if user updated an existing record

                        $("#task" + id).replaceWith( task );
                    }

                    $('#frmTasks').trigger("reset");
                  }
                  else if (path.search('admin/category') > 0) {
                    var category = '';
                    category = '<tr id="category' + data.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + data.category_name + '</td><td>' + data.created_at + '</td>';
                    category += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + data.id + '">Edit</button> ';
                    category += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add"){ //if user added a new record
                        $('#table-list').append(category);
                    }else{ //if user updated an existing record

                        $("#category" + id).replaceWith( category );
                    }

                    $('#frmCategory').trigger("reset");
                  }
                  else if (path.search('admin/profile') > 0){
                    window.location.href = 'profile';
                  }
                  else if (path.search('admin/orders/detail') > 0) {
                    var orders = '';
                    orders = '<tr id="orders' + data.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + data.orders_id + '</td><td>' + data.products_id + '</td><td>' + data.quantity + '</td><td>' + data.price + '</td><td>' + data.sub_total + '</td>';
                    orders += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + data.id + '">Edit</button> ';
                    orders += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add"){ //if user added a new record
                      $('#table-list').append(orders);
                    }else{ //if user updated an existing record

                      $("#orders" + id).replaceWith( orders );
                    }

                    $('#frmOrders').trigger("reset");
                  }
                  else if (path.search('admin/orders') > 0) {
                    var orders = '';
                    orders = '<tr id="orders' + data.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + data.orders_code + '</td><td>' + data.orders_date + '</td><td>' + data.user_id + '</td><td>' + data.total_amount + '</td><td>' + data.status + '</td>';
                    orders += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + data.id + '">Edit</button> ';
                    orders += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add"){ //if user added a new record
                        $('#table-list').append(orders);
                    }else{ //if user updated an existing record

                        $("#orders" + id).replaceWith( orders );
                    }

                    $('#frmOrders').trigger("reset");
                  }

                  $('#myModal').modal('hide');

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      });
    }

});
