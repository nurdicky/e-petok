@extends('admin.layouts.master')

@section('title', 'Categories')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Category</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">category</li>
          </ol>
      </div>
      <div class="col-md-6 col-4 align-self-center">
        <button id="btn-add" name="btn-add" class="btn pull-right hidden-sm-down btn-success">Tambah</button>
          <!-- <a href="{{ url('admin/products/add') }}" class="btn pull-right hidden-sm-down btn-success"> Tambah</a> -->
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
      <!-- column -->
      <div class="col-sm-12">
          <div class="card">
              <div class="card-block">
                  <h4 class="card-title">Table Category</h4>
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th><span class="fa fa-check-square"></span></th>
                                  <th>Category Name</th>
                                  <th>Created At</th>
                                  <th width="15%"></th>
                              </tr>
                          </thead>
                          <tbody id="table-list" name="table-list">
                            <?php $no=1;  ?>
                            @foreach ($categories as $category)
                            <tr id="category{{$category->id}}">
                                <td id="number">
                                  <input type="checkbox" name="id[]" value="22">
                                </td>
                                <td>{{$category->category_name}}</td>
                                <td>{{$category->created_at}}</td>
                                <td>
                                    <button class="btn hidden-sm-down btn-info open-modal" value="{{$category->id}}">Edit</button>
                                    <button class="btn hidden-sm-down btn-danger delete-item" value="{{$category->id}}">Hapus</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Categories Editor</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="frmCategory" name="frmCategory" class="form-horizontal" novalidate="">

                                        <div class="form-group error">
                                            <label for="inputTask" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control has-error" id="category_name" name="category_name" placeholder="Category Name" value="">
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                                    <input type="hidden" id="category_id" name="category_id" value="0">
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>
<meta name="_token" content="{!! csrf_token() !!}" />

@endsection
