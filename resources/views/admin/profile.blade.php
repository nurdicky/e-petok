@extends('admin.layouts.master')

@section('title', 'Profile')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Profile</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
          </ol>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <!-- Row -->
  <div class="row">
      <!-- Column -->
      <div class="col-lg-4 col-xlg-3 col-md-5">
          <div class="card">
              <div class="card-block">
                  <center class="m-t-30"> <img src="{{ asset('admin/assets/images/users/alphabet.png') }}" class="img-circle" width="150" />
                      <h4 class="card-title m-t-10">{{ Auth::user()->name }}</h4>
                      <h6 class="card-subtitle">
                        @if(Auth::user()->status == 1)
                          Administartor
                        @endif
                      </h6>

                  </center>
              </div>
          </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-8 col-xlg-9 col-md-7">
          <div class="card">
              <div class="card-block">
                  <form id="formProfile" class="form-horizontal form-material">
                    <meta name="_token" content="{!! csrf_token() !!}" />
                      <div class="form-group">
                          <label class="col-md-12">Full Name</label>
                          <div class="col-md-12">
                              <input id="fullname" type="text" value="{{ Auth::user()->fullname }}" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="example-email" class="col-md-12">Email</label>
                          <div class="col-md-12">
                              <input id="email" type="email" value="{{ Auth::user()->email }}" class="form-control form-control-line" name="example-email" id="example-email">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Password</label>
                          <div class="col-md-12">
                              <input id="pass" type="password" value="{{ Auth::user()->password }}" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Phone No</label>
                          <div class="col-md-12">
                              <input id="phone" type="text" value="{{ Auth::user()->phone }}" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Address</label>
                          <div class="col-md-12">
                            <input id="address" type="text" value="{{ Auth::user()->address }}" class="form-control form-control-line">
                          </div>
                      </div>

                      <input type="hidden" name="id" id="user_id" value="{{ Auth::user()->id }}">
                      <input type="hidden" name="name" id="name" value="{{ Auth::user()->name }}">

                      <div class="form-group">
                          <div class="col-sm-12">
                              <button class="btn btn-success" id="btn-save" value="update">Update Profile</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- Column -->
  </div>
  <!-- Row -->
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>

@endsection
