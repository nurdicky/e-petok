@extends('admin.layouts.master')

@section('title', 'Dashboard')

@section('content')

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Penjualan Selesai</h4>
                    <div class="text-right">
                        <h2 class="font-light m-b-0"><i class="ti-arrow-up text-success"></i> {{ $countOrders }}</h2>
                        <span class="text-muted">Todays Income</span>
                    </div>
                    <!-- <span class="text-success">80%</span> -->
                    <!-- <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Penjualan Proses</h4>
                    <div class="text-right">
                        <h2 class="font-light m-b-0"><i class="ti-arrow-up text-info"></i> {{ $countOrdersProses }}</h2>
                        <span class="text-muted">Todays Income</span>
                    </div>
                    <!-- <span class="text-info">30%</span> -->
                    <!-- <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- Row -->
    <!-- <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Revenue Statistics</h4>
                    <div class="flot-chart">
                        <div class="flot-chart-content" id="flot-line-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Row -->
    <!-- Row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <select class="custom-select pull-right">
                        <option selected>January</option>
                        <option value="1">February</option>
                        <option value="2">March</option>
                        <option value="3">April</option>
                    </select>
                    <h4 class="card-title">User Paling Sering Beli</h4>
                    <div class="table-responsive m-t-40">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">User</th>
                                    <th>Total Pemesanan</th>
                                    <th>Total Pembelian</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    @if($user->name)
                                    <?php $char = strtoupper(substr($user->name,0,1)); ?>
                                    <td style="width:50px;"><span class="round">{{ $char }}</span></td>
                                    @endif
                                    <td>
                                        <h6>{{ $user->name }}</h6>
                                    </td>
                                    <?php $total = 0; ?>
                                    @foreach($user->orders as $order)
                                      <?php
                                      $amount = $order->total_amount;
                                      $total = (Integer) $total + $amount;
                                      $rupiah = "Rp " . number_format($total,2,',','.');
                                      ?>
                                    @endforeach()
                                    <td>{{ sizeof($user->orders) }}</td>
                                    <td>{{ $rupiah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

@endsection
