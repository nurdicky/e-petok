@extends('admin.layouts.master')

@section('title', 'Products')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Tasks</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Tasks</li>
          </ol>
      </div>
      <div class="col-md-6 col-4 align-self-center">
        <button id="btn-add" name="btn-add" class="btn pull-right hidden-sm-down btn-success">Tambah</button>
          <!-- <a href="{{ url('admin/products/add') }}" class="btn pull-right hidden-sm-down btn-success"> Tambah</a> -->
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
      <!-- column -->
      <div class="col-sm-12">
          <div class="card">
              <div class="card-block">
                  <h4 class="card-title">Table Tasks</h4>
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th><span class="fa fa-check-square"></span></th>
                                  <th>Task Name</th>
                                  <th>Description</th>
                                  <th>Created At</th>
                                  <th width="15%"></th>
                              </tr>
                          </thead>
                          <tbody id="tasks-list" name="tasks-list">
                            <?php $no=1;  ?>
                            @foreach ($tasks as $task)
                            <tr id="task{{$task->id}}">
                                <td id="number">
                                  <input type="checkbox" name="id[]" value="22">
                                </td>
                                <td>{{$task->task}}</td>
                                <td>{{$task->description}}</td>
                                <td>{{$task->created_at}}</td>
                                <td>
                                    <button class="btn hidden-sm-down btn-info open-modal" value="{{$task->id}}">Edit</button>
                                    <button class="btn hidden-sm-down btn-danger delete-item" value="{{$task->id}}">Hapus</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Task Editor</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">

                                        <div class="form-group error">
                                            <label for="inputTask" class="col-sm-3 control-label">Task</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control has-error" id="task" name="task" placeholder="Task" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                                    <input type="hidden" id="task_id" name="task_id" value="0">
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>
<meta name="_token" content="{!! csrf_token() !!}" />

@endsection
