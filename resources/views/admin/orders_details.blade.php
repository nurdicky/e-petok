@extends('admin.layouts.master')

@section('title', 'Order Details')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Order Details</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">order details</li>
          </ol>
      </div>
      <div class="col-md-6 col-4 align-self-center">
        <button id="btn-add" name="btn-add" class="btn pull-right hidden-sm-down btn-success">Tambah</button>
          <!-- <a href="{{ url('admin/products/add') }}" class="btn pull-right hidden-sm-down btn-success"> Tambah</a> -->
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
      <!-- column -->
      <div class="col-sm-12">
          <div class="card">
              <div class="card-block">
                  <h4 class="card-title">Table Order Details</h4>
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th><span class="fa fa-check-square"></span></th>
                                  <th>Orders ID</th>
                                  <th>Products ID</th>
                                  <th>Quantity</th>
                                  <th>Harga</th>
                                  <th>Sub Total</th>
                                  <th width="15%"></th>
                              </tr>
                          </thead>
                          <tbody id="table-list" name="table-list">
                            <?php $no=1;  ?>
                            @foreach ($orders as $order)
                            <tr id="orders{{$order->id}}">
                                <td id="number">
                                  <input type="checkbox" name="id[]" value="22">
                                </td>
                                <td>{{$order->orders_id}}</td>
                                <td>{{$order->products_id}}</td>
                                <td>{{$order->quantity}}</td>
                                <td>{{$order->price}}</td>
                                <td>{{$order->sub_total}}</td>
                                <td>
                                    <button class="btn hidden-sm-down btn-info open-modal" value="{{$order->id}}">Edit</button>
                                    <button class="btn hidden-sm-down btn-danger delete-item" value="{{$order->id}}">Hapus</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Order Details Editor</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="frmOrders" name="frmOrders" class="form-horizontal" novalidate="">

                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Orders ID</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="order_id" name="order_id" placeholder="Order ID" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Products ID</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="products_id" name="products_id" placeholder="Products ID" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Quantity</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="quantity" name="quantity" placeholder="Quantity" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Harga</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="price" name="price" placeholder="Status Orders" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Sub Total</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="sub_total" name="sub_total" placeholder="Sub Total" value="">
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                                    <input type="hidden" id="orders_id" name="orders_id" value="0">
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>
<meta name="_token" content="{!! csrf_token() !!}" />

@endsection
