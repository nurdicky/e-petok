@extends('admin.layouts.master')

@section('title', 'Orders')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Orders</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">orders</li>
          </ol>
      </div>
      <div class="col-md-6 col-4 align-self-center">
        <button id="btn-add" name="btn-add" class="btn pull-right hidden-sm-down btn-success">Tambah</button>
          <!-- <a href="{{ url('admin/products/add') }}" class="btn pull-right hidden-sm-down btn-success"> Tambah</a> -->
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
      <!-- column -->
      <div class="col-sm-12">
          <div class="card">
              <div class="card-block">
                  <h4 class="card-title">Table Orders</h4>
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th><span class="fa fa-check-square"></span></th>
                                  <th>Orders Code</th>
                                  <th>Orders Date</th>
                                  <th>Customer</th>
                                  <th>Total Amount</th>
                                  <th>Status</th>
                                  <th width="15%"></th>
                              </tr>
                          </thead>
                          <tbody id="table-list" name="table-list">
                            <?php $no=1;  ?>
                            @foreach ($orders as $order)
                            <tr id="orders{{$order->id}}">
                                <td id="number">
                                  <input type="checkbox" name="id[]" value="22">
                                </td>
                                <td>{{$order->orders_code}}</td>
                                <td>{{$order->orders_date}}</td>
                                <td>{{$order->user_id}}</td>
                                <td>{{$order->total_amount}}</td>
                                <td>{{$order->status}}</td>
                                <td>
                                    <button class="btn hidden-sm-down btn-info open-modal" value="{{$order->id}}">Edit</button>
                                    <button class="btn hidden-sm-down btn-danger delete-item" value="{{$order->id}}">Hapus</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Orders Editor</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="frmOrders" name="frmOrders" class="form-horizontal" novalidate="">

                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Customer</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="user_id" name="user_id" placeholder="Customer" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Status</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="status" name="status" placeholder="Status Orders" value="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputTask" class="col-sm-12 control-label">Total Amount</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control has-error" id="total_amount" name="total_amount" placeholder="Total Amount" value="">
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                                    <input type="hidden" id="orders_id" name="orders_id" value="0">
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>
<meta name="_token" content="{!! csrf_token() !!}" />

@endsection
