@extends('admin.layouts.master')

@section('title', 'Products')

@section('content')

<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Products</h3>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
          </ol>
      </div>
      <div class="col-md-6 col-4 align-self-center">
        <button id="btn-add" name="btn-add" class="btn pull-right hidden-sm-down btn-success">Tambah</button>
        <!-- <a href="{{ url('admin/products/add') }}" class="btn pull-right hidden-sm-down btn-success"> Tambah</a> -->
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
      <!-- column -->
      <div class="col-sm-12">
          <div class="card">
              <div class="card-block">
                  <h4 class="card-title">Table Products</h4>
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th><span class="fa fa-check-square"></span></th>
                                  <th width="20%">Image</th>
                                  <th>Name</th>
                                  <th>Price</th>
                                  <th>Stock</th>
                                  <th>Category</th>
                                  <th width="15%"></th>
                              </tr>
                          </thead>

                          <tbody id="tabel-list" name="tabel-list">
                            @foreach ($products as $product)
                              <tr id="product{{$product->id}}">
                                  <td ><input type="checkbox" name="id[]" value="22"></td>
                                  <?php $path = URL::to('images/products'); ?>
                                  <?php $image = $product->products_image; ?>
                                  <td><img width="50%" src="<?= $path.'/'.$image ?>"></img></td>
                                  <td>{{ $product->products_name }}</td>
                                  <td>{{ $product->products_price }}</td>
                                  <td>{{ $product->products_stock }}</td>
                                  <td>{{ $product->products_category }}</td>
                                  <td >
                                    <button class="btn hidden-sm-down btn-info open-modal" value="{{$product->id}}">Edit</button>
                                    <button class="btn hidden-sm-down btn-danger delete-item" value="{{$product->id}}">Hapus</button>
                                  </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>

                      <!-- Modal View -->

                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Products Editor</h4>
                                </div>
                                <form action="{{ url('admin/products') }}" enctype="multipart/form-data" method="POST">
                                  <div class="modal-body">
                                      <div class="form-group">
                                          <label for="inputTask" class="col-sm-12 control-label">Products Name</label>
                                          <div class="col-sm-12">
                                              <input type="text" class="form-control" id="products_name" name="products_name" placeholder="Product name" value="">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-12 control-label">Product Price</label>
                                          <div class="col-sm-12">
                                              <input type="text" class="form-control" id="products_price" name="products_price" placeholder="Product price" value="">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label for="inputTask" class="col-sm-12 control-label">Products Stock</label>
                                          <div class="col-sm-12">
                                              <input type="text" class="form-control" id="products_stock" name="products_stock" placeholder="Product stock" value="">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label for="inputTask" class="col-sm-12 control-label">Products Category</label>
                                          <div class="col-sm-12">
                                              <input type="text" class="form-control" id="products_category" name="products_category" placeholder="Product category" value="">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label for="inputTask" class="col-sm-12 control-label">Products Image</label>
                                          <div class="col-sm-12">
                                              <input type="file" class="form-control" id="products_image" name="products_image">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                        <div class="col-sm-12 text-center">
                                          <input type="hidden" name="tempImg" id="tempImg" value="">
                                          <img src="" id="imageView" alt="image">
                                        </div>
                                      </div>

                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <!-- <div id="progress" class="progress">
                                          <div class="progress-bar progress-bar-success"></div>
                                      </div> -->

                                      <!-- <div id="files" class="files"></div> -->


                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary btn-save" type="submit">Save Changes</button>

                                    <!-- <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button> -->
                                    <input type="hidden" id="products_id" name="products_id" value="0">
                                </div>
                                </form>

                            </div>
                        </div>
                      </div>

                      <!-- End Modal view -->

                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End PAge Content -->
  <!-- ============================================================== -->
</div>
<meta name="_token" content="{!! csrf_token() !!}" />

<script>

</script>

@endsection
