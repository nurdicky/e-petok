<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('admin/assets/images/favicon.png') }}">
    <title> @yield('title') | Admin</title>
    <link href="{{ asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/colors/orange.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('admin/css/jquery.fileupload.css') }}" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar card-no-border">

  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
      <svg class="circular" viewBox="25 25 50 50">
          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
      <!-- ============================================================== -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- ============================================================== -->
      <header class="topbar">
          <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
              <!-- ============================================================== -->
              <!-- Logo -->
              <!-- ============================================================== -->
              <div class="navbar-header">
                  <a class="navbar-brand" href="{{ url('admin/dashboard')}}">
                      <!-- Logo icon -->
                      <b style="color: orange">
                          <img width="75%" src="{{ asset('images/home/petok.png') }}" alt="E-Petok">
                      </b>

                  </a>
              </div>
              <!-- ============================================================== -->
              <!-- End Logo -->
              <!-- ============================================================== -->
              <div class="navbar-collapse">
                  <!-- ============================================================== -->
                  <!-- toggle and nav items -->
                  <!-- ============================================================== -->
                  <ul class="navbar-nav mr-auto mt-md-0 ">
                      <!-- This is  -->
                      <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                      <li class="nav-item hidden-sm-down">
                          <form class="app-search p-l-20">
                              <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a>
                          </form>
                      </li>
                  </ul>
                  <!-- ============================================================== -->
                  <!-- User profile and search -->
                  <!-- ============================================================== -->
                  <ul class="navbar-nav my-lg-0">
                      <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="{{ url('admin/profile') }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('admin/assets/images/users/alphabet.png') }}" alt="user" class="profile-pic m-r-5" />
                            {{ @Auth::user()->name }}
                          </a>
                      </li>
                  </ul>
              </div>
          </nav>
      </header>
      <!-- ============================================================== -->
      <!-- End Topbar header -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <aside class="left-sidebar">
          <!-- Sidebar scroll-->
          <div class="scroll-sidebar">
              <!-- Sidebar navigation-->
              <nav class="sidebar-nav">
                  <ul id="sidebarnav">
                      <li>
                          <a href="{{ url('admin/dashboard') }}" class="waves-effect"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Dashboard</a>
                      </li>
                      <li>
                          <a href="{{ url('admin/profile') }}" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Profile</a>
                      </li>
                      <li>
                          <a href="{{ url('admin/products') }}" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Products Table</a>
                      </li>
                      <li>
                          <a href="{{ url('admin/category') }}" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Category Table</a>
                      </li>
                      <li>
                          <a href="{{ url('admin/orders') }}" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Orders Table</a>
                      </li>
                      <li>
                          <a href="{{ url('admin/orders/detail') }}" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Order Details Table</a>
                      </li>
                      <li>
                          <a href="{{ url('logout') }}" class="waves-effect"><i class="fa fa-sign-out m-r-10" aria-hidden="true"></i>Logout</a>
                      </li>
                  </ul>
              </nav>
              <!-- End Sidebar navigation -->
          </div>
          <!-- End Sidebar scroll-->
      </aside>
      <!-- ============================================================== -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper  -->
      <!-- ============================================================== -->
      <div class="page-wrapper">

          @yield('content')

          <!-- footer -->
          <!-- ============================================================== -->
          <footer class="footer text-center">
              © 2017 <b>E-Petok</b> template by Monster Admin
          </footer>
          <!-- ============================================================== -->
          <!-- End footer -->
          <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Page wrapper  -->
      <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Wrapper -->
  <!-- ============================================================== -->


  <script src="{{ asset('admin/assets/plugins/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
  <script src="{{ asset('admin/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- slimscrollbar scrollbar JavaScript -->
  <script src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
  <!--Wave Effects -->
  <script src="{{ asset('admin/js/waves.js') }}"></script>
  <!--Menu sidebar -->
  <script src="{{ asset('admin/js/sidebarmenu.js') }}"></script>
  <!--stickey kit -->
  <script src="{{ asset('admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
  <!--Custom JavaScript -->
  <script src="{{ asset('admin/js/custom.min.js') }}"></script>
  <!-- ============================================================== -->
  <!-- This page plugins -->
  <!-- ============================================================== -->
  <!-- Flot Charts JavaScript -->
  <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.js') }}"></script>
  <script src="{{ asset('admin/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
  <script src="{{ asset('admin/js/flot-data.js') }}"></script>
  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <script src="{{ asset('admin/assets/plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
  <script src="{{ asset('admin/js/ajax-crud.js') }}"></script>
  <script src="{{ asset('admin/js/jquery.ui.widget.js') }}"></script>
  <script src="{{ asset('admin/js/jquery.fileupload.js') }}"></script>
</body>

</html>
