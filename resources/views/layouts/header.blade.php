  <header id="header"><!--header-->
  <div class="header_top"><!--header_top-->
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="contactinfo">
            <ul class="nav nav-pills">
            </ul>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div><!--/header_top-->

  <div class="header-middle"><!--header-middle-->
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="logo pull-left">
            <a href="{{ url('/home') }}"><img src="{{ asset('images/home/petok.png') }}" alt="" /></a>
          </div>
        </div>
        <div class="col-sm-8 pull-right">
          <br><br>
          <div class="shop-menu pull-right">
            <ul class="nav navbar-nav" id="menu">
              @if(@!Auth::user()->name)
              <li><a href="{{ url('home') }}"><i class="fa fa-home"></i> Home</a></li>
              <li><a href="{{ url('register') }}"><i class="fa fa-user"></i> Daftar</a></li>
              <li><a href="{{ url('login') }}"><i class="fa fa-lock"></i> Masuk</a></li>
              @else
              <li><a href="{{ url('cart') }}"><i  class="fa fa-shopping-cart"></i> Keranjang Belanja</a></li>
              <li><a href="{{ url('account') }}"><i class="fa fa-gear"></i> Akun <b>({{ Auth::user()->name }}) </b></a></li>
              <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Keluar</a></li>
              @endif
            </ul>
          </div>
        </div>
      </div>
      <hr style="margin-top: -5px; border: 0.5px solid #dcdcdc;">
    </div>
  </div><!--/header-middle-->

  <?php
  $split = explode("/", Request::url());
  if (!@$split[3]) {
    $url = 'home';
  }else{
    $url = $split[3];
  }
  ?>

  @if(@$url != 'login' && @$url != 'register')
  <div class="header-bottom" style="padding-top:0"><!--header-bottom-->
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="mainmenu pull-left">
            <ul class="nav navbar-nav collapse navbar-collapse">
              <li><a href="{{ url('/home') }}" id="home">Beranda</a></li>
              <li><a href="{{ url('/panduan') }}" id="panduan">Panduan Belanja</a></li>
              <li><a href="{{ url('/investasi') }}" id="investasi">Investasi</a></li>
              <li><a href="{{ url('/about') }}" id="about">Tentang Kami</a></li>
            </ul>
          </div>
        </div>
        <!-- <div class="col-sm-3">
          <div class="search_box pull-right">
            <input type="text" placeholder="Pencarian Produk"/>
          </div>
        </div> -->
      </div>
    </div>
  </div><!--/header-bottom-->
  @endif
</header><!--/header-->
