<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title') | E-Petok</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}">
    <link rel="stylesheet" href="{{ asset('css/price-range.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    <?php
    $split = explode("/", Request::url());
    $url = $split[3];
    ?>

    @if(@$url == 'investasi')
    <!-- Bootstrap core CSS     -->
    <link href="https://vestifarm.com/dashboard/asset_template/creative-tim/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="https://vestifarm.com/dashboard/asset_template/creative-tim/css/material-dashboard.css?v=1.2.0" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="https://vestifarm.com/dashboard/asset_template/creative-tim/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <script src="https://vestifarm.com/dashboard/asset_template/creative-tim/js/jquery-3.1.0.min.js" type="text/javascript"></script>
    <script src="https://vestifarm.com/dashboard/asset_template/dist/js/jquery.number.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>


    <style>
        .need-logo{
            background: url(https://vestifarm.com/dashboard/asset_template/creative-tim/img/logo.png) 100% 100% no-repeat;
            background-size: cover;
        }

        .cont-invest{
            padding-top:25px;
        }
    </style>
    @endif

</head><!--/head-->
@if(@$url == 'investasi')
<body style="background-color: #fff">
@else
<body>
@endif

  @include('layouts.header')

  @yield('content')

  @include('layouts.footer')

</body>
</html>
