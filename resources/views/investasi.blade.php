@extends('layouts.master')

@section('title', 'Investasi')

@section('content')

   <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">
	    		<div class="col-sm-12">
					<h2 class="title text-center">INVESTASI DENGAN<strong> KAMI</strong></h2>

    <div class="container-fluid">
        <div class="row" style="padding:0px">
            <div class="col-md-12">
                <div class="card">
	                    <div class="card-header" data-background-color="orange">
	                        <h4 class="card-title">Bagaimana invest E-PETHOK bekerja?</h4>
	                    </div>
	                    <div class="card-content">
						<h4> <br> </h4>
	                        <div class="row">
	                            <center><div class="col-md-3">
	                                <img class="img" src="images/home/pesan.jpg" style="width:200px;height:200px;" />
	                                <h4>Order Melalui Telepon</h4>
	                                <p>Pesan ayam (satu ekor ayam atau lebih) hanya dengan telepon ke nomor yang ada di kontak person untuk melakukan investasi dan deal langsung</p>

	                            </div></center>

	                            <center><div class="col-md-3">
	                                <img class="img" src="images/home/ayamcilik.png" style="width:150px;height:200px;" />
	                                <h4>Beli Ayam</h4>
	                                <p>Pembelian ayam di E-PETHOK</p>

	                            </div></center>

								<center><div class="col-md-3">
	                                <img class="img" src="images/home/dibesarkan.jpg" style="width:200px;height:200px;" />
	                                <h4>Ayam Dibesarkan</h4>
	                                <p>Oleh profesional yang akan menjaga kesehatan dan pertumbuhan ayam sampai layak dijual</p>

	                            </div></center>

	                            <center><div class="col-md-3">
	                                <img class="img" src="images/home/ayambesar.jpg" style="width:200px;height:200px;" />
	                                <h4>Ayam Dijual</h4>
	                                <p>Penjualan ayam dilakukan ketika ayam siap untuk dipasarkan</p>
	                            </div></center>

                                <center><div class="col-md-3 col-md-offset-2" style="margin-right:100px;">
	                                <img class="img" src="images/home/profit.png" style="width:200px;height:200px;" />
	                                <h4>Sharing-Profit</h4>
	                                <p>50:50 masing-masing untuk anda dan E-PETHOK</p>
	                            </div></center>
	                        </div>

			         </div>
			     </div>
            </div>
            <div class="col-md-12" style="padding-top:20px">
                    <div class="card">
	                    <div class="card-header" data-background-color="blue">
	                        <h4 class="card-title">Terms & Condition</h4>
	                    </div>
								<div class="card-content">
									<div class="row">
										<ol>
											<li>Nominal investasi untuk 1 slot (1 ekor sapi terdiri dari 6 slot) adalah senilai Rp 2.750.000, dengan jangka waktu investasi selama 4 bulan. <br><br></li>

											<li>Satu ekor sapi terdiri dari 6 slot. Anda bebas menentukan berapapun jumlah slot yang ingin Anda investasikan. Slot Anda akan digabung dengan slot investor lain dengan sistem kolektif.<br><br></li>

											<li>Vestifarm menggunakan sistem kolektif, dimana seluruh sapi yang nantinya dirawat adalah milik bersama seluruh investor dan Vestifarm, sesuai dengan slot yang diinvestasikan. Tidak ada investor maupun Vestifarm yang memiliki sapi tertentu secara spesifik.<br><br></li>

											<li>Pembagian keuntungan menggunakan akad bagi hasil (Profit & Loss Sharing). Dimana pembagian keuntungan antara Vestifarm dan Investor adalah 50:50. Adapun estimasi return pada periode #InvestQurban kali ini adalah 8-18%. <b><br>PENTING: bukan fixed-return,</b> akadnya adalah bagi hasil, apapun yang terjadi akan ditanggung bersama oleh Vestifarm & Investor, baik untung maupun rugi. <br><br></li>

											<li>Masa periode investasi adalah 4 bulan, keuntungan beserta modal awal akan Anda terima setelah sapi terjual di bulan ke-4.<br><br></li>

											<li>Lokasi perawatan sapi berlokasi di Subang, Jawa Barat. Dikelola oleh peternak professional yang berpengalaman lebih dari 8 tahun, langsung di bawah manajemen Vestifarm.<br><br></li>

											<li>Setiap sapi yang dirawat akan diasuransikan. Sehingga apabila terjadi kematian pada sapi tsb, pihak asuransi akan melakukan penggantian 85% dari harga beli. Hal ini sebagai mitigasi risiko kematian yang mungkin terjadi di dalam masa perawatan. Sehingga dapat meminimalisasi kerugian yang akan ditanggung bersama.<br><br></li>

											<li>Simulasi perhitungan keuntungan (1 slot).
												<br>1 ekor sapi terdiri dari 6 slot)
												<br>Profit rata-rata 1 sapi = 3 juta rupiah
												<br>Profit untuk investor 50% = 1,5 juta rupiah
												<br>Profit 1 slot = 1,5 juta / 6 slot = Rp 250.000
												<br><br><br>
												Return Anda  	= modal awal + profit 1 slot
												<br>= Rp.2.750.000 + 250.000
												<br>= Rp. 3.000.000 (+ 9,1%)<br><br>

												<b>*catatan : </b><br>
												a. Jika lebih dari 1 slot, keuntungan yang didapat hanya tinggal dikali dengan keuntungan 1 slot. <br>
												b. Angka profit rata-rata 1 sapi di atas adalah estimasi. Bisa lebih atau kurang dari angka tersebut.
											</li>
										</ol>
									</div>
								</div>
				            </div>
		      </div>

							</div>
						</div>
					</div>
				</section></center>
				<br>
			</div>
			</div>
			</div>

@endsection
