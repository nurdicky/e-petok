@extends('layouts.master')

@section('title', 'Home')

@section('content')


<section id="slider"><!--slider-->
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#slider-carousel" data-slide-to="1"></li>
            <li data-target="#slider-carousel" data-slide-to="2"></li>
          </ol>

          <div class="carousel-inner">
            <div class="item active">
              <div class="col-sm-6">
                <h1><span>E</span>-Pethok</h1>
              </div>
              <div class="col-sm-6">
                <img src="images/home/peternakan.jpg" class="girl img-responsive" alt="" style="height: 340px;" />

              </div>
            </div>
            <div class="item">
              <div class="col-sm-6">
                <h1><span>E</span>-Pethok</h1>

              </div>
              <div class="col-sm-6">
                <img src="images/home/peternakandua.jpg" class="girl img-responsive" alt="" />

              </div>
            </div>

            <div class="item">
              <div class="col-sm-6">
                <h1><span>E</span>-Pethok</h1>

              </div>

              <div class="col-sm-6">
                <img src="images/home/peternakantiga.jpg" class="girl img-responsive" alt="" />

              </div>
            </div>

          </div>

          <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
            <i class="fa fa-angle-left"></i>
          </a>
          <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
            <i class="fa fa-angle-right"></i>
          </a>
        </div>

      </div>
    </div>
  </div>
</section><!--/slider-->

<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="left-sidebar">
          <h2>Kategori</h2>
          <div class="panel-group category-products" id="accordian"><!--category-productsr-->
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordian" href="#ayam">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Ayam Peternak
                  </a>
                </h4>
              </div>
              <div id="ayam" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul>
                    <li><a href="detail.php">Ayam Broiler </a></li>
                    <li><a href="detail.php">Ayam Kampung </a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordian" href="#telur">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Ayam Petelur
                  </a>
                </h4>
              </div>
              <div id="telur" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul>
                    <li><a href="detail.php">Telur Ayam Broiler </a></li>
                    <li><a href="detail.php">Telur Ayam Kampung </a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordian" href="#daging">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    Daging Ayam
                  </a>
                </h4>
              </div>
              <div id="daging" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul>
                    <li><a href="detail.php">Daging Ayam Broiler </a></li>
                    <li><a href="detail.php">Daging Ayam Kampung </a></li>
                  </ul>
                </div>
              </div>
            </div>

          </div><!--/price-range-->

        </div>
      </div>

      <div class="col-sm-9 padding-right">
        <div class="features_items"><!--features_items-->
          <h2 class="title text-center">PRODUK KITA</h2>
          @foreach($products as $product)
          <div class="col-sm-4">
            <div class="product-image-wrapper">
              <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="images/products/{{ $product->products_image }}" height="200" width="100" alt="" />
                    <h6>{{ $product->products_name }}</h6>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                      <?php
                      $harga = $product->products_price;
                      $harga = 'Rp. '. $harga . ',-/kg'
                      ?>
                      <h2>{{ $harga }}</h2>
                      <?php $id = $product->id ?>
                      <?php $url = 'products/detail/' ?>
                      <?php $url = $url.$id ?>
                      <a href="{{ url($url) }}" class="btn btn-default add-to-cart" ><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          @endforeach

        </div><!--features_items-->



        <div class="recommended_items"><!--recommended_items-->
          <h2 class="title text-center">Produk Rekomendasi</h2>

          <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="item active">
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/boilerdua.jpg" height="200" alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Daging Ayam Broiler per-Bagian</h8>

                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/boilerempat.png" height="200"  alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Ayam Broiler Potong</h8>

                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/telurkampung.jpg" height="200" alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Telur Ayam Kampung Sekeranjang</h8>

                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/kampungan.jpg" height="200" alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Ayam Kampung Potong</h8>

                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/paha.jpg" height="200" alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Ayam Broiler Potong</h8>
                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="product-image-wrapper">
                    <div class="single-products">
                      <div class="productinfo text-center">
                        <img src="images/home/daging.jpg" height="200" alt="" />
                        <br>
                        <br>
                        <h8 style="color: #FE980F;">Daging Ayam Broiler Fillet</h8>

                        <a href="detail.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail Barang</a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
             <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
              <i class="fa fa-angle-left"></i>
              </a>
              <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
              <i class="fa fa-angle-right"></i>
              </a>
          </div>
        </div><!--/recommended_items-->

      </div>
    </div>
  </div>
</section>

@endsection
