@extends('layouts.master')

@section('title', 'Checkout')

@section('content')

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Pembayaran</h2>
			</div>

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<div class="form-one">
								<form>
									<input type="text" placeholder="Email*">
									<input type="text" placeholder="Nama Depan *">
									<input type="text" placeholder="Nama Tengah">
									<input type="text" placeholder="Nama BElakang *">
									<input type="text" placeholder="Alamat 1 *">
									<input type="text" placeholder="Alamat 2">
								</form>
							</div>
							<div class="form-two">
								<form>
									<input type="text" placeholder="Code Pos *">
									<select>
										<option>-- Negara --</option>
										<option>Indonesia</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<select>
										<option>-- Provinsi --</option>
										<option>Jawa Timur</option>
										<option>Jawa Barat</option>
										<option>Jawa Tengah</option>
									</select>
									<select>
										<option>-- Kota --</option>
										<option>Surabaya</option>
										<option>Jombang</option>
										<option>Mojokerto</option>
									</select>
									<input type="password" placeholder="Komfirm password">
									<input type="text" placeholder="No HP *">

							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Keterangan Tambahan</p>
							<textarea name="message"  placeholder="Tuliskan keterangan tambahan, apabila ada" rows="16"></textarea>
							<label><input type="checkbox">Keterangan Tambahan</label>
						</div>
					</div>
				</div>
			</div>
			<div class="review-payment">
				<h2>Pembayaran</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Harga</td>
							<td class="quantity">Jumlah</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/home/telurkampung.jpg" alt="" style="width:200px;height:200px"></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Telur Ayam Kampung</a></h4>

							</td>
							<td class="cart_price">
								<p>Rp 1.000</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">Rp 1.000</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>

						<tr>
							<td class="cart_product">
								<a href=""><img src="images/home/boiler.jpg" alt="" style="width:200px;height:200px"></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Ayam Boiler Potong</a></h4>

							</td>
							<td class="cart_price">
								<p>Rp 35.000</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">Rp 35.000</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Total Pembayaran</td>
										<td>Rp 35.100</td>
									</tr>
									<tr class="shipping-cost">
										<td>Jasa Pengiriman</td>
										<td>Gratis</td>
									</tr>
									<tr>
										<td>Total</td>
										<td><span>Rp 35.100</span></td>
									</tr>


								</table>
								<div class="overlay-content" >
									<a href="index.php" class="btn btn-default add-to-cart" style="width:200px;">Bayar</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	</section> <!--/#cart_items-->

@endsection
