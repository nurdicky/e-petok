@extends('layouts.master')

@section('title', 'Investasi')

@section('content')

	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">
	    		<div class="col-sm-12">
					<h2 class="title text-center">Tentang<strong> Perusahaan</strong></h2>
    			<center>

            <section>
    					<div class="container">
    						<div class="row">
    							<div class="col-sm-3">
    								&nbsp;
    							</div>

    							<div class="col-sm-3 pull-right">
    								&nbsp;
    							</div>

    							<div class="col-sm-6">
    								<div class="product-image-wrapper" style="">
    									<div class="single-products">
    										<div class="productinfo">
    											<img src="images/home/peternakantiga.jpg" style="height:100%;width:100%;" alt="" />
    											<h2 >Apa Itu E-PETHOK?</h2>
    			            					<p style="text-align: justify;">E-Pethok adalah supplier ayam peternak dan ayam petelur serta telur dan daging ayam potong. Kami siap melayani Anda kapanpun dan dimanapun. Siap mengirim dan memasok kebutuhan daging ayam ke tempat anda baik di seluruh kota di Jawa Timur, Jakarta, Bandung, Depok, Bekasi, Tangerang atau kota lainnya di Indonesia. </p>
    			            					<p style="text-align: justify;"> Saat ini E-Pethok melihat potensi penjualan atau pemasaran produk baik berupa barang atau jasa melalui internet, sehingga kami tertarik untuk mencoba memperluas pasar penjualan ayam peternak dan petelur kami ke seluruh kota di Indonesia khususnya di Jawa Timur.</p>
    			            					<p style="text-align: justify;"> Beberapa insudtri produksi yang dapat kita suplai seperti :
    			            					<li style="text-align: left;">- Supply semua jenis daging ayam ke seluruh Industri Makanan</li>
        										<li style="text-align: left;">- Restaurant Seluruh Jawa Timur</li>
        										<li style="text-align: left;">- Cafe – cafe di Jawa Timur</li>
        										<li style="text-align: left;">- Rumah Sakit</li>
        										<li style="text-align: left;">- Hotel</li>
        										<li style="text-align: left;">- Caterer</li>
        										<li style="text-align: left;">- Beberapa kantor dan pabrik di Jakarta</li>
        										<li style="text-align: left;">- dan masih banyak lagi.</li></p>

    											<p style="text-align: justify;">Saat ini kami juga sudah bisa menyediakan daging ayam fillet (boneless dada, boneless paha), parting, sampingan  (kepala, ati ampela, jeroan) dan produk daging ayam yang dibutuhkan pasar lainnya. Jika anda ingin bekerjasama dengan kami silahkan lihat halaman Kerjasama Usaha. Jika anda mempunyai usaha seperti pecel ayam, gorengan ayam, fried chicken, restoran. Tetapi anda terhambat dengan pasokan bahan baku yang kurang baik? KFayam juga dapat menawarkan kerjasama dengan anda. Jika anda penjual ingin berbagi suplai dengan kami silahkan bisa juga kontak kami untuk membahasnya lebih lanjut.</p>
    										</div>
    									</div>
    								</div>

    								<div class="container" style="margin-left:250px;">
    			        				<div class="row">

    			       					 </div>
    			     				</div>
    							</div>
    						</div>
    					</div>
    				</section>
          </center>
				<br>
				<br>
				<br>
				<br>
    		<div class="row">
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Hubungi Kami</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Subject">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
				            </div>
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Kontak Info</h2>
	    				<address style="text-align: center;">
	    					<p>E-PETHOK </p>
							<p>Jawa Timur</p>
							<p>No HP: +2346 17 38 93</p>
							<p>Email: info@e-pethok.com</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Media Sosial Kami</h2>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>
	    	</div>
    	</div>
    </div><!--/#contact-page-->

@endsection
