@extends('layouts.master')

@section('title', 'Panduan')

@section('content')


	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active">
									<img src="images/home/cara1.png" class="girl img-responsive" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!--/slider-->

@endsection
