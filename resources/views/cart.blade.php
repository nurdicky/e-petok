@extends('layouts.master')

@section('title', 'Keranjang Belanja')

@section('content')


	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">

			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Harga</td>
							<td class="quantity">Jumlah</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
            @foreach($carts as $item)

						<tr>
							<td class="cart_product">
								<a href=""><img width="75" src="images/products/{{ $item->products->products_image }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">  {{ $item->products->products_name }} </a></h4>
							</td>
							<td class="cart_price">
								<p>{{ $item->products->products_price }}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{ $item->quantity }}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{ $item->sub_total }}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
            @endforeach

						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<h4>Total</h4> <td><span>Rp. 35.100</span></td>
									</tr>

								</table>
								<center>
								<a href="checkout.html" class="btn btn-default add-to-cart"><b><u>LANJUTKAN PEMBAYARAN</u></b></a></center>
							</td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>


@endsection
