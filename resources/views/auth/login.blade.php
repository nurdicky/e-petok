@extends('layouts.master')

@section('title', 'Login')

@section('content')

<section>
  <div class="container">
    <div class="row">
      <br><br>
      <div class="col-sm-4 col-sm-offset-4">
        <div class="login-form"><!--login form-->
          <h2 style="text-align: center;">Masuk E-Petok</h2>
              <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}
                  <input placeholder="Email-Address" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                  <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>
    							<span>
    								<input type="checkbox" class="checkbox">
    								Ingat saya
    							</span> <a href="#" style="padding-left: 47%;">Lupa Kata Sandi</a>
    							<button type="submit" class="btn btn-default" style="width: 100%; height: 40px;">Login</button>
                  <h2 style="font-size: 15px; text-align: center;">Belum punya akun E-Petok? Daftar<a href="{{ url('register') }}"> di sini</a></h2>
    					</form>
        </div><!--/login form-->
      </div>
    </div>
  </div>
</section><!--/form-->
@endsection
