@extends('layouts.master')

@section('title', 'Daftar')

@section('content')

<section><!--form-->
  <div class="container">
    <div class="row"><br>
      <div class="col-sm-4 col-sm-offset-4">
        <div class="signup-form"><!--sign up form-->
          <h2 style="text-align: center;">Silahkan Daftar</h2>
          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <input placeholder="Nama" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
            <input placeholder="Email Address" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>
            <input placeholder="Confirm-Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

            <button type="submit" class="btn btn-default" style="width: 100%; height: 40px;">Signup</button>
            <h2 style="font-size: 15px; text-align: center;">Sudah punya akun E-Petok? Masuk <a href="{{ url('login')}}"> di sini</a></h2>
          </form>
        </div><!--/sign up form-->
      </div>
    </div>
  </div>
</section><!--/form-->


@endsection
